package labyrinth;

import java.util.ArrayList;

import cellular.cellstate.ICellState;
import datastructure.Grid;
import datastructure.IGrid;
import labyrinth.gui.LabyrinthGUI;

public class Main {
	// Labyrint
	public static char[][] testLabyrint = { //
			{ '*', '*', '*', '*' }, //
			{ '*', ' ', ' ', '*' }, //
			{ '*', ' ', '*', '*' }, //
			{ '*', 's', '*', '*' }, //
			{ '*', '*', '*', '*' }, };

	public static void main(String[] args) {
		// LabyrinthGUI.run(() -> new
		// Labyrinth(LabyrinthHelper.loadGrid(testLabyrint)));
		LabyrinthGUI.run(() -> new Labyrinth(LabyrinthHelper.makeRandomGrid(20, 30)));


	}
}
